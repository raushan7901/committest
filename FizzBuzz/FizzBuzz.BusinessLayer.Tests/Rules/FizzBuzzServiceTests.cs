﻿namespace FizzBuzz.BusinessLayer.Tests.Rules
{
    using System.Collections.Generic;
    using FizzBuzz.BusinessLayer.Interfaces;
    using FizzBuzz.BusinessLayer.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class FizzBuzzServiceTests
    {
        private FizzBuzzService fizzBuzzService;
        private Mock<IDivide> mockDivisibilityByThree;
        private Mock<IDivide> mockDivisibilityByFive;
        private IEnumerable<IDivide> divisibility;
        private IEnumerable<string> expectedOutputList;
        private int inputNumber;

        [SetUp]
        public void TestSetup()
        {
            mockDivisibilityByThree = new Mock<IDivide>();
            mockDivisibilityByFive = new Mock<IDivide>();
            inputNumber = 15;
            divisibility = new List<IDivide>() { mockDivisibilityByThree.Object, mockDivisibilityByFive.Object };
            fizzBuzzService = new FizzBuzzService(divisibility);
            mockDivisibilityByThree.Setup(m => m.IsDivisible(It.Is<int>(x => x % 3 == 0))).Returns(true);
            mockDivisibilityByFive.Setup(m => m.IsDivisible(It.Is<int>(x => x % 5 == 0))).Returns(true);
            mockDivisibilityByThree.Setup(m => m.IsDivisible(It.Is<int>(x => x % 3 != 0))).Returns(false);
            mockDivisibilityByFive.Setup(m => m.IsDivisible(It.Is<int>(x => x % 5 != 0))).Returns(false);
        }

        [Test]
        public void FizzBuzzList_ReturnFizzBuzzTextForNotSpecifiedDay_OnInvoking()
        {
            // Arrange
            IEnumerable<string> fizzBuzzList = new List<string>();
            expectedOutputList = new List<string>() { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz" };
            mockDivisibilityByThree.Setup(m => m.FizzBuzzDisplayText()).Returns("Fizz");
            mockDivisibilityByFive.Setup(m => m.FizzBuzzDisplayText()).Returns("Buzz");

            // Act
            fizzBuzzList = fizzBuzzService.FizzBuzzList(inputNumber);

            // Assert
            CollectionAssert.AreEquivalent(expectedOutputList, fizzBuzzList);
        }

        [Test]
        public void FizzBuzzList_ReturnWizzWuzzTextForSpecifiedDay_OnInvoking()
        {
            // Arrange
            IEnumerable<string> fizzBuzzList = new List<string>();
            expectedOutputList = new List<string>() { "1", "2", "WIZZ", "4", "WUZZ", "WIZZ", "7", "8", "WIZZ", "WUZZ", "11", "WIZZ", "13", "14", "WIZZ WUZZ" };
            mockDivisibilityByThree.Setup(m => m.FizzBuzzDisplayText()).Returns("WIZZ");
            mockDivisibilityByFive.Setup(m => m.FizzBuzzDisplayText()).Returns("WUZZ");

            // Act
            fizzBuzzList = fizzBuzzService.FizzBuzzList(inputNumber);

            // Assert
            CollectionAssert.AreEquivalent(expectedOutputList, fizzBuzzList);
        }
    }
}
