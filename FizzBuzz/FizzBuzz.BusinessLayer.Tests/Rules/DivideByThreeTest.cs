﻿namespace FizzBuzz.BusinessLayer.Tests.Rules
{
    using FizzBuzz.BusinessLayer.Interfaces;
    using FizzBuzz.BusinessLayer.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivideByThreeTest
    {
        private DivideByThree divideByThree;
        private Mock<IDayChecker> mockDayChecker;

        [SetUp]
        public void SetUpTest()
        {
            mockDayChecker = new Mock<IDayChecker>();
            divideByThree = new DivideByThree(mockDayChecker.Object);
        }

        [TestCase(3, true)]
        [TestCase(7, false)]
        public void DivisibilityByThree_ShouldReturnTrueOrFalse__WhenNumberProvidedIsDivisibleByThreeOrNot(int inputNumber, bool expectedOutput)
        {
            // Act
            bool actualOutput = divideByThree.IsDivisible(inputNumber);

            // Assert
            actualOutput.Should().Be(expectedOutput);
        }

        [TestCase(true, "WIZZ")]
        [TestCase(false, "FIZZ")]
        public void FizzBuzzDisplayText_ShouldReturnWizzFizz_WhenDayIsSpecifiedOrNot(bool specifiedDay, string expectedOutput)
        {
            // Arrange
            mockDayChecker.Setup(m => m.IsSpecifiedDay(It.IsAny<string>())).Returns(specifiedDay);

            // Act
            var result = divideByThree.FizzBuzzDisplayText();

            // Assert
            result.Should().Be(expectedOutput);
        }
    }
}
