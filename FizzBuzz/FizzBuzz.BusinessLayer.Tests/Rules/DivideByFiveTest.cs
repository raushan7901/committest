﻿namespace FizzBuzz.BusinessLayer.Tests.Rules
{
    using FizzBuzz.BusinessLayer.Interfaces;
    using FizzBuzz.BusinessLayer.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DivideByFiveTest
    {
        private Mock<IDayChecker> mockDayChecker;
        private DivideByFive divideByFive;

        [SetUp]
        public void TestSetup()
        {
            mockDayChecker = new Mock<IDayChecker>();
            divideByFive = new DivideByFive(mockDayChecker.Object);
        }

        [TestCase(5, true)]
        [TestCase(7, false)]
        public void DivisibilityByFive_ShouldReturnTrueOrFalse_WhenNumberProvidedIsDivisibleByFiveOrNot(int inputNumber, bool expectedResult)
        {
            // Act
            var result = divideByFive.IsDivisible(inputNumber);

            // Assert
            result.Should().Be(expectedResult);
        }

        [TestCase(true, "WUZZ")]
        [TestCase(false, "BUZZ")]
        public void FizzBuzzDisplayText_ShouldReturnWuzzBuzz_WhenDayIsSpecifiedOrNot(bool specifiedDay, string expectedOutput)
        {
            // Arrange
            mockDayChecker.Setup(m => m.IsSpecifiedDay(It.IsAny<string>())).Returns(specifiedDay);

            // Act
            var result = divideByFive.FizzBuzzDisplayText();

            // Assert
            result.Should().Be(expectedOutput);
        }
    }
}
