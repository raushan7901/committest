﻿namespace FizzBuzz.BusinessLayer.Tests.Rules
{
    using FizzBuzz.BusinessLayer.Rules;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class DayCheckerTest
    {
        private DayChecker dayChecker;

        [SetUp]
        public void SetUpTest()
        {
            dayChecker = new DayChecker();
        }

        [TestCase("Wednesday", true)]
        [TestCase("Monday", false)]
        public void DayChecker_ReturnsTrueOrFalse_WhenDayIsSpecifiedOrNot(string specifiedDay, bool expectedOutput)
        {
            // Act
            var actualOutput = dayChecker.IsSpecifiedDay(specifiedDay);

            // Assert
            actualOutput.Should().Be(expectedOutput);
        }
    }
}
