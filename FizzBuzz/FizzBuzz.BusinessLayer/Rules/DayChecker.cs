﻿namespace FizzBuzz.BusinessLayer.Rules
{
    using System.Configuration;
    using FizzBuzz.BusinessLayer.Interfaces;

    public class DayChecker : IDayChecker
    {
        public bool IsSpecifiedDay(string specifiedDay)
        {
            return specifiedDay == ConfigurationManager.AppSettings["BusinessDay"];
        }
    }
}