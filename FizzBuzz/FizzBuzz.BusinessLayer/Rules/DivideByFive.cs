﻿namespace FizzBuzz.BusinessLayer.Rules
{
    using System;
    using FizzBuzz.BusinessLayer.Interfaces;

    public class DivideByFive : IDivide
    {
        private readonly IDayChecker dayChecker;

        public DivideByFive(IDayChecker dayChecker)
        {
            this.dayChecker = dayChecker;
        }

        public bool IsDivisible(int value)
        {
            return value % 5 == 0;
        }

        public string FizzBuzzDisplayText()
        {
            return dayChecker.IsSpecifiedDay(DateTime.Now.DayOfWeek.ToString()) ? Constants.DivisibleByFiveSpecifiedDay : Constants.DivisibleByFiveNotSpecifiedDay;
        }
    }
}