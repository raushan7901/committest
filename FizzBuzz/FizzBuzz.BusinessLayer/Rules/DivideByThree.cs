﻿namespace FizzBuzz.BusinessLayer.Rules
{
    using System;
    using FizzBuzz.BusinessLayer.Interfaces;

    public class DivideByThree : IDivide
    {
        private readonly IDayChecker dayChecker;

        public DivideByThree(IDayChecker dayChecker)
        {
            this.dayChecker = dayChecker;
        }

        public bool IsDivisible(int value)
        {
            return value % 3 == 0;
        }

        public string FizzBuzzDisplayText()
        {
            return this.dayChecker.IsSpecifiedDay(DateTime.Now.DayOfWeek.ToString()) ? Constants.DivisibleByThreeSpecifiedDay : Constants.DivisibleByThreeNotSpecifiedDay;
        }
    }
}