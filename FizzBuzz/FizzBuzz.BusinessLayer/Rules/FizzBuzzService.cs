﻿namespace FizzBuzz.BusinessLayer.Rules
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzz.BusinessLayer.Interfaces;

    public class FizzBuzzService : IFizzBuzzService
    {
        private readonly IEnumerable<IDivide> divisionList;

        public FizzBuzzService(IEnumerable<IDivide> divisionList)
        {
            this.divisionList = divisionList;
        }

        public IEnumerable<string> FizzBuzzList(int number)
        {
            var fizzBuzzList = new List<string>();
            for (int index = 1; index <= number; index++)
            {
                var isDivisible = this.divisionList.Where(m => m.IsDivisible(index)).ToList();
                fizzBuzzList.Add(isDivisible.Any() ? string.Join(" ", isDivisible.Select(m => m.FizzBuzzDisplayText())) : index.ToString());
            }

            return fizzBuzzList;
        }
    }
}