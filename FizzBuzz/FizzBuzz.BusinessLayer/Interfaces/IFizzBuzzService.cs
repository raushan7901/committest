﻿namespace FizzBuzz.BusinessLayer.Interfaces
{
    using System.Collections.Generic;

    public interface IFizzBuzzService
    {
        IEnumerable<string> FizzBuzzList(int number);
    }
}
