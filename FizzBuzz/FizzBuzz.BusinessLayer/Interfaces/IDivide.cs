﻿namespace FizzBuzz.BusinessLayer.Interfaces
{
    public interface IDivide
    {
        bool IsDivisible(int value);

        string FizzBuzzDisplayText();
    }
}
