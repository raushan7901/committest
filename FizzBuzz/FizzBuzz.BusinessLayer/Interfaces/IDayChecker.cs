﻿namespace FizzBuzz.BusinessLayer.Interfaces
{
    public interface IDayChecker
    {
        bool IsSpecifiedDay(string specifiedDay);
    }
}
