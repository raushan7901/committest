﻿namespace FizzBuzz.BusinessLayer
{
    public class Constants
    {
        public const string DivisibleByThreeNotSpecifiedDay = "FIZZ";
        public const string DivisibleByFiveNotSpecifiedDay = "BUZZ";
        public const string DivisibleByThreeSpecifiedDay = "WIZZ";
        public const string DivisibleByFiveSpecifiedDay = "WUZZ";
    }
}